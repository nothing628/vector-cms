const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass("resources/css/app.sass", "public/css", {}, [require("tailwindcss")]);
mix.js("resources/js/app.js", "public/js");
mix.ts("resources/js/vue.ts", "public/js").vue({ version: 3 }).webpackConfig((t, config) => {
    // console.log(config.plugins);
    // console.log(config.module.rules[5].use[0].options.plugins);
    console.log(config);

    return {}
});
