import { mount } from "@cypress/vue";
import Button from "./button.vue";

it("Button", () => {
    // with JSX
    mount(() => <Button>Test button</Button>);

    // ... or ...
    // mount(Button, {
    //     slots: {
    //         default: "Test button",
    //     },
    // });

    cy.get("button").contains("Test button").click();
});
