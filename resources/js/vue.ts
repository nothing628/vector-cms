import { createApp } from "vue";
import App from "./components/app.vue";

const myApp = createApp(App);

myApp.mount("#app");
