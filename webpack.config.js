const { VueLoaderPlugin } = require("vue-loader");
const babel = require("@babel/core");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

module.exports = {
    mode: "development",
    entry: {
        "/js/app": [
            __dirname + "/resources/js/app.js",
            __dirname + "/resources/css/app.sass",
        ],
        "/js/vue": [__dirname + "/resources/js/vue.ts"],
    },
    plugins: [new VueLoaderPlugin()],
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: [
                    {
                        loader: require.resolve("vue-loader"),
                        options: {},
                    },
                ],
            },
            {
                test: /\.html$/,
                resourceQuery: { not: [/\?vue/i] },
                use: [
                    {
                        loader: require.resolve("html-loader"),
                    },
                ],
            },
            {
                test: /(\.(png|jpe?g|gif|webp|avif)$|^((?!font).)*\.svg$)/,
                use: [
                    {
                        loader: require.resolve("file-loader"),
                        options: {
                            /**
                             * @param {string} path
                             */
                            name: (path) => {
                                if (
                                    !/node_modules|bower_components/.test(path)
                                ) {
                                    return "img" + "/[name].[ext]?[hash]";
                                }

                                return (
                                    "img" +
                                    "/vendor/" +
                                    path
                                        .replace(/\\/g, "/")
                                        .replace(
                                            /((.*(node_modules|bower_components))|images|image|img|assets)\//g,
                                            ""
                                        ) +
                                    "?[hash]"
                                );
                            },
                            publicPath: "/",
                        },
                    },
                    {
                        loader: require.resolve("img-loader"),
                        options: {},
                    },
                ],
            },
            {
                test: /(\.(woff2?|ttf|eot|otf)$|font.*\.svg$)/,
                use: [
                    {
                        loader: require.resolve("file-loader"),
                        options: {
                            name: (path) => {
                                if (
                                    !/node_modules|bower_components/.test(path)
                                ) {
                                    return "img" + "/[name].[ext]?[hash]";
                                }

                                return (
                                    "img" +
                                    "/vendor/" +
                                    path
                                        .replace(/\\/g, "/")
                                        .replace(
                                            /((.*(node_modules|bower_components))|images|image|img|assets)\//g,
                                            ""
                                        ) +
                                    "?[hash]"
                                );
                            },
                            publicPath: "/",
                        },
                    },
                ],
            },
            {
                test: /\.(cur|ani)$/,
                use: [
                    {
                        loader: require.resolve("file-loader"),
                        options: {
                            name: "[name].[ext]?[hash]",
                            publicPath: "/",
                        },
                    },
                ],
            },
            {
                test: /\.(cjs|mjs|jsx?|tsx?)$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: require.resolve("babel-loader"),
                        options: {
                            cacheDirectory: true,
                            presets: [
                                babel.createConfigItem(
                                    [
                                        "@babel/preset-env",
                                        {
                                            modules: false,
                                            forceAllTransforms: true,
                                        },
                                    ],
                                    { type: "preset" }
                                ),
                            ],
                            plugins: [
                                babel.createConfigItem(
                                    "@vue/babel-plugin-jsx",
                                    { type: "plugin" }
                                ),
                                babel.createConfigItem(
                                    "@babel/plugin-syntax-dynamic-import",
                                    { type: "plugin" }
                                ),
                                babel.createConfigItem(
                                    "@babel/plugin-proposal-object-rest-spread",
                                    { type: "plugin" }
                                ),
                                babel.createConfigItem(
                                    [
                                        "@babel/plugin-transform-runtime",
                                        {
                                            helpers: false,
                                        },
                                    ],
                                    { type: "plugin" }
                                ),
                            ],
                            assumptions: {},
                            targets: {},
                            cloneInputAst: true,
                            babelrc: true,
                            configFile: true,
                            browserslistConfigFile: false,
                            passPerPreset: false,
                            envName: "development",
                            cwd: "/home/titan/project/appkulo/vector-cms",
                            root: "/home/titan/project/appkulo/vector-cms",
                            rootMode: "root",
                            filename: undefined,
                            babelrcRoots: [
                                ".",
                                "/home/titan/project/appkulo/vector-cms",
                            ],
                        },
                    },
                ],
            },
            {
                test: /\.tsx?$/,
                loader: require.resolve("ts-loader"),
                exclude: /node_modules/,
                options: {
                    appendTsSuffixTo: [/\.vue$/],
                },
            },
        ],
    },
};
